﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Grpc.Core;
using MadEyeMatt.AspNetCore.Authorization.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;

namespace Genius_Server.Services;

public class Authentication : AuthenticationService.AuthenticationServiceBase
{
    public override Task<AuthResult> Authenticate(Grant request, ServerCallContext context)
    {
        var token = CreateToken(request.Username, "admin", "user:read");

        var result = new AuthResult();

        if (request.Username == "admin")
        {
            result.Token = token;

            SentrySdk.AddBreadcrumb(
                message: "Authenticated user " + request.Username,
                category: "auth",
                level: BreadcrumbLevel.Info);
        }
        else
            throw new RpcException(new(StatusCode.Unauthenticated, request.Username));

        return Task.FromResult(result);
    }

    [Authorize]
    public override Task<AuthResult> RefreshToken(RefreshTokenRequest request, ServerCallContext context)
    {
        var handler = new JwtSecurityTokenHandler();

        var oldToken = handler.ReadJwtToken(request.Token);

        var permissions = oldToken.Claims.Where(_ => _.Type == PermissionClaimTypes.PermissionClaimType);
        var role = oldToken.Claims.First(_ => _.Type == "roles").Value;

        var newToken = CreateToken(oldToken.Subject, role, permissions);

        return Task.FromResult(new AuthResult {Token = newToken});
    }

    private static string CreateToken(string username, string role, params string[] permissions)
    {
        return CreateToken(username, role,
            permissions.Select(_ => new Claim(PermissionClaimTypes.PermissionClaimType, _)));
    }

    private static string CreateToken(string username, string role, IEnumerable<Claim> permissions)
    {
        var claims = new List<Claim>
        {
            new("roles", role),
            new(JwtRegisteredClaimNames.Sub, username),
            new(JwtRegisteredClaimNames.Aud, "Genius"),
            new(JwtRegisteredClaimNames.UniqueName, username),
            new(JwtRegisteredClaimNames.Iat, DateTime.Now.Ticks.ToString(), ClaimValueTypes.Integer64)
        };
        claims.AddRange(permissions);

        var creds = new SigningCredentials(JwtTokenValidator.Key, SecurityAlgorithms.HmacSha256);
        var token = new JwtSecurityToken("Furesoft", expires: DateTime.Today.AddDays(300),
            claims: claims, signingCredentials: creds);

        var handler = new JwtSecurityTokenHandler();

        return handler.WriteToken(token);
    }
}