using System.Collections.Concurrent;
using System.Text.Json;
using GeniusServer.Protos;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace Genius_Server.Services;

[Authorize]
public class PubSubServiceImpl : PubSubService.PubSubServiceBase
{
    private readonly static ConcurrentDictionary<string, List<IServerStreamWriter<Message>>> subscribers = new ConcurrentDictionary<string, List<IServerStreamWriter<Message>>>();

    public override async Task Subscribe(SubscriptionRequest request, IServerStreamWriter<Message> responseStream, ServerCallContext context)
    {
        var topic = request.Topic;

        subscribers.AddOrUpdate(topic, new List<IServerStreamWriter<Message>> { responseStream }, (key, list) =>
        {
            list.Add(responseStream);

            return list;
        });

        try
        {
            context.CancellationToken.WaitHandle.WaitOne();
        }
        finally
        {
            subscribers.AddOrUpdate(topic, new List<IServerStreamWriter<Message>>(), (key, list) =>
            {
                list.Remove(responseStream);

                return list;
            });
        }
    }

    public override async Task<Success> Publish(PublishRequest request, ServerCallContext context)
    {
        if (subscribers.TryGetValue(request.Topic, out var subscriberList))
        {
            foreach (var subscriber in subscriberList)
            {
                await subscriber.WriteAsync(new Message { Content = request.Content });
            }
        }

        return new Success();
    }

    public override Task<Success> Unsubscribe(SubscriptionRequest request, ServerCallContext context)
    {
        var topic = request.Topic;

        subscribers.AddOrUpdate(topic, new List<IServerStreamWriter<Message>>(), (key, list) =>
        {
            list.Clear();
            return list;
        });

        return Task.FromResult(new Success());
    }

    public async void PublishMessage(string topic, object content)
    {
        if (subscribers.TryGetValue(topic, out var subscriberList))
        {
            foreach (var subscriber in subscriberList)
            {
                var serialized = JsonConvert.SerializeObject(content, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                });

                await subscriber.WriteAsync(new Message { Content = serialized });
            }
        }
    }
}