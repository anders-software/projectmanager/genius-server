using Genius_Server.Core.ProfileImageProviders;
using GeniusServer.Protos;
using Google.Protobuf;
using Grpc.Core;
using IdenticonSharp;
using IdenticonSharp.Identicons;
using IdenticonSharp.Identicons.Defaults.GitHub;
using MadEyeMatt.AspNetCore.Authorization.Permissions;
using Microsoft.AspNetCore.Authorization;
using SixLabors.ImageSharp.Formats.Jpeg;

namespace Genius_Server.Services;

[Authorize]
public class Profile : ProfileService.ProfileServiceBase
{
    static Profile()
    {
        IdenticonManager.ConfigureDefault<GitHubIdenticonProvider, GitHubIdenticonOptions>(options =>
        {
            options.Background = new(240, 240, 240);
            options.SpriteSize = 10;
            options.Size = 256;
            options.HashAlgorithm = HashProvider.SHA512;
        });
        IdenticonManager.ConfigureDefault<GravatarProvider, GravatarProviderOptions>(options =>
        {
            options.Background = new(240, 240, 240);
            options.Style = GravatarStyle.monsterid;
        });
    }

    [RequirePermission("user:read")]
    public override Task<GetProfileResponse> GetProfileImage(EmptyMessage request, ServerCallContext context)
    {
        var user = context.GetHttpContext().User;

        var ms = new MemoryStream();
        var img = IdenticonManager.Get<GravatarProvider>().Create(user!.Identity!.Name);
        img.Save(ms, new JpegEncoder());
        ms.Seek(0, SeekOrigin.Begin);

        return Task.FromResult(new GetProfileResponse {Data = ByteString.FromStream(ms)});
    }

    [RequirePermission("user:read")]
    public override Task<AccountMessage> GetProfileInformation(EmptyMessage request, ServerCallContext context)
    {
        return Task.FromResult(new AccountMessage() { Email = "admin@admin.tld", Username = "hanna123"});
    }
}