﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using Genius_Server.Core.ExceptionHandling;
using Genius_Server.Services;
using MadEyeMatt.AspNetCore.Authorization.Permissions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.OpenApi.Models;
using Sentry.AspNetCore;
using Sentry.AspNetCore.Grpc;
using Sentry.Profiling;

namespace Genius_Server;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.WebHost.ConfigureKestrel(options =>
        {
            // Setup a HTTP/2 endpoint without TLS.
            options.ListenLocalhost(5000, o => o.Protocols = HttpProtocols.Http2);
        });

        AddSentry(builder);
        AddGRPC(builder);
        AddAuthentication(builder);
        AddSwagger(builder);
        AddHealthChecks(builder);

        var app = builder.Build();

        Configure(app);

        app.Run();
    }

    private static void AddSentry(WebApplicationBuilder builder)
    {
        builder.WebHost.UseSentry(builder =>
        {
            builder.AddGrpc();
            builder.AddSentryOptions(options =>
            {
                options.Dsn = "https://3fabc6d1b1a6c3ddfed4d307d35a57d3@o1405618.ingest.sentry.io/4506757671813120";
                // The parameter 'options' here has values populated through the configuration system.
                // That includes 'appsettings.json', environment variables and anything else
                // defined on the ConfigurationBuilder.
                // See: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.1&tabs=basicconfiguration
                // Tracks the release which sent the event and enables more features: https://docs.sentry.io/learn/releases/
                // If not explicitly set here, the SDK attempts to read it from: AssemblyInformationalVersionAttribute and AssemblyVersion
                // TeamCity: %build.vcs.number%, VSTS: BUILD_SOURCEVERSION, Travis-CI: TRAVIS_COMMIT, AppVeyor: APPVEYOR_REPO_COMMIT, CircleCI: CIRCLE_SHA1
                options.Release =
                    "e386dfd"; // Could also be any format, such as: 2.0, or however version of your app is

                options.EnableTracing = true;

                options.MaxBreadcrumbs = 200;

                options.AddIntegration(new ProfilingIntegration());

                // Set a proxy for outgoing HTTP connections
                options.HttpProxy = null; // new WebProxy("https://localhost:3128");

                // Example: Disabling support to compressed responses:
                options.DecompressionMethods = DecompressionMethods.None;

                options.MaxQueueItems = 100;
                options.ShutdownTimeout = TimeSpan.FromSeconds(5);

                options.Debug = false;
                options.StackTraceMode = StackTraceMode.Enhanced;
                options.SampleRate = 1.0f; // Not recommended in production - may adversely impact quota
                options.TracesSampleRate = 1.0f; // Not recommended in production - may adversely impact quota
                // Initialize some (non null) ExperimentalMetricsOptions to enable Sentry Metrics,
                options.ExperimentalMetrics = new ExperimentalMetricsOptions
                {
                    EnableCodeLocations = true, // Set this to false if you don't want to track code locations
                    CaptureSystemDiagnosticsInstruments = [
                        // Capture System.Diagnostics.Metrics matching the name "HatCo.HatStore", which is the name
                        // of the custom HatsMeter defined above
                        "hats-sold"
                    ],
                    // Capture all built in metrics (this is the default - you can override this to capture some or
                    // none of these if you prefer)
                    CaptureSystemDiagnosticsMeters = BuiltInSystemDiagnosticsMeters.All
                };

                // Configures the root scope
                options.ConfigureScope(s => s.SetTag("Always sent", "this tag"));
            });
        });
    }

    private static void Configure(WebApplication app)
    {
        app.UseAuthentication();
        app.UseAuthorization();

        app.MapGrpcHealthChecksService();

        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "Genius Server API V1");
        });

        app.MapGrpcService<Services.Authentication>();
        app.MapGrpcService<Profile>();
        app.MapGrpcService<PubSubServiceImpl>();

        app.MapGrpcReflectionService();

        var protoService = app.Services.GetRequiredService<ProtoController>();

        MapSingleProto(app, protoService);
        MapProtoIndex(app, protoService);

        app.UseSentryTracing();
    }

    private static void MapProtoIndex(WebApplication app, ProtoController protoService)
    {
        app.MapGet("/", async context =>
        {
            var filenames = protoService.GetAvailable();

            var links = string.Join('\n',
                filenames.Select(_ =>
                    $"<a href=http://{$"{context.Request.Host}/protos/{_}"}>{Path.GetFileNameWithoutExtension(_)}<a/>"));

            await context.Response.WriteAsync($"<html><head><title>Services</title></head><body>{links}</body></html>");
        });
    }

    private static void MapSingleProto(WebApplication app, ProtoController protoService)
    {
        app.MapGet("/protos/{protoName}", async context =>
        {
            var protoName = (string) context.Request.RouteValues["protoName"];

            var content = protoService.Get(protoName);

            if (content != null)
                await content.CopyToAsync(context.Response.Body);
            else
                context.Response.StatusCode = (int) HttpStatusCode.NotFound;
        });
    }

    private static void AddHealthChecks(WebApplicationBuilder builder)
    {
        builder.Services.AddGrpcHealthChecks()
            .AddCheck("Sample", () => HealthCheckResult.Healthy());
    }

    private static void AddSwagger(WebApplicationBuilder builder)
    {
        builder.Services.AddGrpcSwagger();
        builder.Services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1",
                new OpenApiInfo { Title = "Genius Server API", Version = "v1" });

            var filePath = Path.Combine(AppContext.BaseDirectory, "Genius Server.xml");
            c.IncludeXmlComments(filePath);
            c.IncludeGrpcXmlComments(filePath, includeControllerXmlComments: true);
        });
    }

    private static void AddAuthentication(WebApplicationBuilder builder)
    {
        builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;

                o.TokenValidationParameters.ValidIssuer = "Furesoft";
                o.TokenValidationParameters.ValidAudience = "Genius";
                o.TokenValidationParameters.IssuerSigningKey = JwtTokenValidator.Key;

                o!.TokenHandlers.Add(new JwtSecurityTokenHandler());
            });

        builder.Services.AddAuthorization();
        builder.Services.AddPermissionsAuthorization();
    }

    private static void AddGRPC(WebApplicationBuilder builder)
    {
        builder.Services.AddGrpc(options =>
        {
            options.Interceptors.Add<ExceptionInterceptor>();
        }).AddJsonTranscoding();

        builder.Services.AddGrpcReflection();

        builder.Services.AddSingleton<ProtoController>();
    }
}