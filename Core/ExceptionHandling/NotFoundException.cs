using Grpc.Core;

namespace Genius_Server.Core.ExceptionHandling;

public class NotFoundException : RpcException
{
    public NotFoundException(string message)
        : base(new(StatusCode.NotFound, message))
    {
    }

    public NotFoundException(Status status) : base(status)
    {
    }

    public NotFoundException(Status status, string message) : base(status, message)
    {
    }

    public NotFoundException(Status status, Metadata trailers) : base(status, trailers)
    {
    }

    public NotFoundException(Status status, Metadata trailers, string message) : base(status, trailers, message)
    {
    }
}