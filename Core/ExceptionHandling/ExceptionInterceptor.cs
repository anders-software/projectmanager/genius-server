using Grpc.Core;
using Grpc.Core.Interceptors;

namespace Genius_Server.Core.ExceptionHandling;

public class ExceptionInterceptor : Interceptor
{
    private static RpcException Rewire(Exception exception, ServerCallContext context)
    {
        return exception switch
        {
            RpcException rpcException => rpcException,
            NotImplementedException => CreateExcepion(StatusCode.Unimplemented, exception),
            _ => CreateExcepion(StatusCode.Internal, exception)
        };
    }

    private static RpcException CreateExcepion(StatusCode code, Exception exception)
    {
        return new(new(code, exception.Message));
    }

    public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
        TRequest request,
        ServerCallContext context,
        UnaryServerMethod<TRequest, TResponse> continuation)
    {
        try
        {
            return await continuation(request, context);
        }
        catch (Exception e)
        {
            throw Rewire(e, context);
        }
    }

    public override async Task<TResponse> ClientStreamingServerHandler<TRequest, TResponse>(
        IAsyncStreamReader<TRequest> requestStream,
        ServerCallContext context,
        ClientStreamingServerMethod<TRequest, TResponse> continuation)
    {
        try
        {
            return await continuation(requestStream, context);
        }
        catch (Exception e)
        {
            throw Rewire(e, context);
        }
    }


    public override async Task ServerStreamingServerHandler<TRequest, TResponse>(
        TRequest request,
        IServerStreamWriter<TResponse> responseStream,
        ServerCallContext context,
        ServerStreamingServerMethod<TRequest, TResponse> continuation)
    {
        try
        {
            await continuation(request, responseStream, context);
        }
        catch (Exception e)
        {
            throw Rewire(e, context);
        }
    }

    public override async Task DuplexStreamingServerHandler<TRequest, TResponse>(
        IAsyncStreamReader<TRequest> requestStream,
        IServerStreamWriter<TResponse> responseStream,
        ServerCallContext context,
        DuplexStreamingServerMethod<TRequest, TResponse> continuation)
    {
        try
        {
            await continuation(requestStream, responseStream, context);
        }
        catch (Exception e)
        {
            throw Rewire(e, context);
        }
    }
}