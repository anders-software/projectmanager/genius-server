using System.Security.Cryptography;
using System.Text;
using IdenticonSharp.Identicons;
using IdenticonSharp.Svg;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace Genius_Server.Core.ProfileImageProviders;

public enum GravatarStyle
{
    Default,
    mp,
    identicon,
    monsterid,
    wavatar,
    retro,
    robohash
}

public class GravatarProviderOptions : IIdenticonOptions
{
    public GravatarStyle Style { get; set; } = GravatarStyle.monsterid;
    public Rgba32 Background { get; set; }
}

public class GravatarProvider : IIdenticonProvider<GravatarProviderOptions>
{
    public GravatarProviderOptions Options => new();

    public Image<Rgba32> Create(string input)
    {
        var hash = CalculateHash(input.Trim().ToLower());

        if (Options.Style != GravatarStyle.Default)
            hash += "?d=" + Enum.GetName(Options.Style);

        var webclient = new HttpClient();
        var url = $"https://gravatar.com/avatar/{hash}";

        var response = webclient.GetAsync(url);

        return Image.Load(response.Result.Content.ReadAsStream());
    }

    public Image<Rgba32> Create(byte[] input)
    {
        return Create(Encoding.Default.GetString(input));
    }

    public SvgBuilder CreateSvg(string input)
    {
        throw new NotImplementedException();
    }

    public SvgBuilder CreateSvg(byte[] input)
    {
        throw new NotImplementedException();
    }

    public bool ProvidesSvg => false;

    public static string CalculateHash(string input)
    {
        if (input == null)
            throw new InvalidOperationException("The input parameter is required.");
        // step 1, calculate MD5 hash from input
#pragma warning disable SCS0006 // Weak hashing function.
#pragma warning restore SCS0006 // Weak hashing function.
        var inputBytes = Encoding.ASCII.GetBytes(input);
        var hash = SHA256.HashData(inputBytes);

        // step 2, convert byte array to hex string
        var sb = new StringBuilder();
        foreach (var t in hash) sb.Append(t.ToString("X2"));

        return sb.ToString().ToLower();
    }
}