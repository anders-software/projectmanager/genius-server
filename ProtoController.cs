﻿using System.Reflection;

namespace Genius_Server;

public class ProtoController
{
    public Stream Get(string protoName)
    {
        var assembly = Assembly.GetEntryAssembly();

        return assembly!.GetManifestResourceStream($"{typeof(ProtoController).Namespace}.Protos.{protoName}.proto")!;
    }

    public IEnumerable<string> GetAvailable()
    {
        return Assembly.GetEntryAssembly()!.GetManifestResourceNames().Select(_ => _.Split('.')[^2]);
    }
}
